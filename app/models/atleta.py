class Atleta:
    def __init__(self,nome,slug,atleta_id,rodada_id,posicao_id,clube_id,pontos_num,preco_num,variaca_num,media_num,clube_id_full_name,DS,FC,FD,FF,FS,G,I,PI,CA,SG,DD,GS,A,CV,FT,GC,PP):
         self.nome = nome
         self.slug = slug
         self.atleta_id = atleta_id
         self.rodada_id = []
         self.posicao_id = posicao_id
         self.clube_id = clube_id
         self.pontos_num = []
         self.preco_num = []
         self.variaca_num = []
         self.media_num = []
         self.clube_id_full_name = clube_id_full_name
         self.DS = DS
         self.FC = FC
         self.FD = FD
         self.FF = FF
         self.FS = FS
         self.G = G
         self.I = I
         self.PI = PI
         self.CA = CA
         self.SG = SG
         self.DD = DD
         self.GS = GS
         self.A = A
         self.CV = CV
         self.FT = FT
         self.GC = GC
         self.PP = PP

    def add(self,rodada_id,pontos_num,preco_num,variaca_num,media_num):
         self.rodada_id.append(rodada_id)
         self.pontos_num.append(pontos_num)
         self.preco_num.append(preco_num)
         self.variaca_num.append(variaca_num)
         self.media_num.append(media_num)
from app import app,mongo
import json
from bson.json_util import dumps
from flask import request,jsonify
from bson.objectid import ObjectId


@app.route('/rodadas/add', methods=['POST'])
def add_rodada():
    data = request.json
    for person in data['rodadas']:
        _rodada = person['rodada']       

        if _rodada and request.method == 'POST':   
            id = mongo.db.rodadas_db.insert({'rodada':_rodada})

    resp = jsonify("Rodada added successfully")

    resp.status_code = 200

    return resp

@app.route('/rodadas/list', methods=['GET'])
def get_rodadas():
    rodadas = mongo.db.rodadas_db.find()
    rodada_list = []
    for rodada in rodadas:
        rodada_list.append(rodada['rodada'])

    resp = dumps(rodada_list)    
    return resp
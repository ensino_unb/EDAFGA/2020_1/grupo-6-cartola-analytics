from app import app, mongo
import json
from bson.json_util import dumps
from flask import request,jsonify
from bson.objectid import ObjectId
from app.models.atleta import Atleta
from app.models.rodadas import Rodadas
from app.models.Stack import Stack
from app.models.List import linkedList
from flask import render_template

Jogadores = linkedList()
atleta = mongo.db.atletas_db.find()
for atletas in atleta:
    Jogadores.append(atletas)

RankTemporada = linkedList()
temp = mongo.db.atletas_db.find().sort(f'media_num.2',-1).limit(10)
for atletas in temp:
    RankTemporada.append(atletas)

RankRodada = linkedList()
rodada = mongo.db.atletas_db.find().sort(f'pontos_num.2',-1).limit(10)
for atletas in rodada:
    RankRodada.append(atletas)

Jogador = Jogadores[300]
MyTEAM = linkedList()
rodadaAtual = 2
pilha = Stack()
preco = Stack()
preco.push(0)
dadosGraph1 = [0,1,2,3,4,5,]
#rotas dos paginas
@app.route('/jogadorInfo')
def info():
    return render_template(
        'graficos.html',
        Jogador=Jogador,
    )
#rota index
@app.route('/')
def index():
    return render_template(
        'index.html',
        )

#rota sei lá

@app.route('/tab')
def tab():
    return render_template(
        'pesquisa.html',
        jogadores=Jogadores,
        RankTemporada=RankTemporada,
        RankRodada=RankRodada,
        MyTeam=MyTEAM,
        Pilha=pilha,
        rodadaAtual=rodadaAtual,
        jogador=Jogador,
        dadosGraph1=dadosGraph1,
        preco=preco.peek()
        )
@app.route('/myteam/add/<atletaId>', methods=['POST'])
def add_player(atletaId):
    tmp = Jogadores.index(atletaId)
    total = 0
    MyTEAM.append(tmp)
    for atl in MyTEAM:
        total = total + atl['preco_num'][0]
    preco.push(round(total,2))    
    return "ok"

@app.route('/myteam/remove/<atletaId>', methods=['POST'])
def remove_player(atletaId):
    tmp = Jogadores.index(atletaId)
    MyTEAM.remove(tmp)
    pilha.push(tmp)
    return "ok"

@app.route('/myteam/readd', methods=['POST'])
def readd_player():
    tmp = pilha.pop()
    MyTEAM.append(tmp)
    return "ok"

@app.route('/jogador/set/<atletaId>', methods=['POST'])
def set_player(atletaId):
    tmp = Jogadores.index(atletaId)
    Jogador = tmp
    resp = dumps(Jogador) 
    return resp


@app.route('/add', methods=['POST'])
def add_user():
    try:
        data = request.json
        x = len(data)
        print(x)
        i = 0
        for person in range(x):
            print(i)
            i += 1
            _nome = data[person]['atletas.nome']
            _slug = data[person]['atletas.slug']
            _atleta_id = data[person]['atletas.atleta_id']
            _rodada_id = data[person]['atletas.rodada_id']
            _posicao_id = data[person]['atletas.posicao_id']
            _clube_id = data[person]['atletas.clube_id']
            _pontos_num = data[person]['atletas.pontos_num']
            _preco_num = data[person]['atletas.preco_num']
            _variaca_num = data[person]['atletas.variacao_num']
            _media_num = data[person]['atletas.media_num']
            _clube_id_full_name = data[person]['atletas.clube.id.full.name']
            _DS = data[person]['DS']
            _FC = data[person]['FC']
            _FD = data[person]['FD']
            _FF = data[person]['FF']
            _FS = data[person]['FS']
            _G = data[person]['G']
            _I = data[person]['I']
            _PI = data[person]['PI']
            _CA = data[person]['CA']
            _SG = data[person]['SG']
            _DD = data[person]['DD']
            _GS = data[person]['GS']
            _A = data[person]['A']
            _CV = data[person]['CV']
            _FT = data[person]['FT']
            _GC = data[person]['GC']
            _PP = data[person]['PP']
            NewAtleta = Atleta(
                _nome,
                _slug,
                _atleta_id,
                _rodada_id,
                _posicao_id,
                _clube_id,
                _pontos_num,
                _preco_num,
                _variaca_num,
                _media_num,
                _clube_id_full_name,
                _DS,
                _FC,
                _FD,
                _FF,
                _FS,
                _G,
                _I,
                _PI,
                _CA,
                _SG,
                _DD,
                _GS,
                _A,
                _CV,
                _FT,
                _GC,
                _PP
                
            )
            atletaBanco = get_atleta(_atleta_id)
            if (atletaBanco is None) and request.method == 'POST':  
                id = mongo.db.atletas_db.insert(
                            {'nome':_nome,
                            'slug' : _slug,
                            'atleta_id' : _atleta_id,
                            'rodada_id' : [_rodada_id],
                            'posicao_id' : _posicao_id,
                            'clube_id' : _clube_id,
                            'pontos_num' : [_pontos_num],
                            'preco_num' : [_preco_num],
                            'variaca_num' : [_variaca_num],
                            'media_num' : [_media_num],
                            'clube_id_full_name' : _clube_id_full_name,
                            'DS': _DS,
                            'FC': _FC,
                            'FD': _FD,
                            'FF': _FF,
                            'FS': _FS,
                            'G': _G,
                            'I': _I,
                            'PI': _PI,
                            'CA': _CA,
                            'SG': _SG,
                            'DD': _DD,
                            'GS': _GS,
                            'A': _A,
                            'CV': _CV,
                            'FT': _FT,
                            'GC': _GC,
                            'PP': _PP}         
                            
                            )
                resp = jsonify("User added successfully")
                resp.status_code = 200
                
            else:
                #Adicionar os valores do array no novo atleta 
                num_rodadas = len(atletaBanco['rodada_id'])
                
                for tmp in range(num_rodadas):
                    NewAtleta.add(
                        atletaBanco['rodada_id'][tmp],
                        atletaBanco['pontos_num'][tmp],
                        atletaBanco['preco_num'][tmp],
                        atletaBanco['variaca_num'][tmp],
                        atletaBanco['media_num'][tmp]
                    )
                #Adicionar os novos valores
                NewAtleta.add(
                        _rodada_id,
                        _pontos_num,
                        _preco_num,
                        _variaca_num,
                        _media_num,     
                    )   
                #Apagar o atleta antigo
                get_atleta_by_id_and_delete(_atleta_id)
                #Salvar o novo atleta no banco
                
                id = mongo.db.atletas_db.insert(
                            {'nome':NewAtleta.nome,
                            'slug' : NewAtleta.slug,
                            'atleta_id':NewAtleta.atleta_id,
                            'rodada_id' : NewAtleta.rodada_id,
                            'posicao_id' : NewAtleta.posicao_id,
                            'clube_id' : NewAtleta.clube_id,
                            'pontos_num' : NewAtleta.pontos_num,
                            'preco_num' : NewAtleta.preco_num,
                            'variaca_num' : NewAtleta.variaca_num,
                            'media_num' : NewAtleta.media_num,
                            'clube_id_full_name' : NewAtleta.clube_id_full_name,
                            "DS":_DS,
                            "FC": _FC,
                            "FD": _FD,
                            "FF": _FF,
                            "FS": _FS,
                            "G": _G,
                            "I": _I,
                            "PI": _PI,
                            "CA": _CA,
                            "SG": _SG,
                            "DD": _DD,
                            "GS": _GS,
                            "A": _A,
                            "CV": _CV,
                            "FT": _FT,
                            "GC": _GC,
                            "PP": _PP}         
                            
                            )
                resp = jsonify("User updated successfully")
                resp.status_code = 206           
        return resp
    except:
        not_found()            
def get_atleta(id):
    atleta = mongo.db.atletas_db.find_one({'atleta_id':id})
    return atleta
def get_atleta_by_id_and_delete(id):
    mongo.db.atletas_db.delete_one({'atleta_id':id})
    resp = jsonify("Atleta deletado com sucesso")
    resp.status_code = 200

@app.route('/atletas', methods=['GET'])
def set_linkedList_atletas():
    try:       
        return render_template('index.html',Jogadores=Jogadores,titulo='Marcos')
    except:
        not_found()

@app.route('/rankRodada/<int:rodada>', methods=['GET'])
def get_rankRodada(rodada):
    try:
        rodada = rodada-1
        atleta = mongo.db.atletas_db.find().sort(f'pontos_num.{rodada}',-1).limit(10)
        print(Jogadores._size)
        resp = dumps(atleta)
        return resp
    except:
        not_found()

@app.route('/rankTemp/<int:rodada>', methods=['GET'])
def get_rankTemp(rodada):
    try:
        rodada = rodada-1
        atleta = mongo.db.atletas_db.find().sort(f'media_num.{rodada}',-1).limit(10)
        resp = dumps(atleta)
        return resp
    except:
        not_found()             
@app.errorhandler(404)
def not_found(error=None):
    message = {
        'status':404,
        'message':'Not Found' + request.url
    }
    resp = jsonify(message)
    
    resp.status_code = 404

    return resp
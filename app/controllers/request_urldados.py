# -*- coding: utf-8 -*-
from tornado.options import options, define
from requests.auth import HTTPBasicAuth
import tornado.ioloop
import tornado.httpserver
import tornado.web
import asyncio
import json
import os
from bs4 import BeautifulSoup      # Raspar elementos de páginas da internet
import requests 


rodadas_cadastradas = ["rodada-1",
                        "rodada-2",
                        "rodada-3",
                        "rodada-4",
                        "rodada-5",
                        "rodada-6",
                        "rodada-7",
                        "rodada-8",
                        "rodada-9",
                        "rodada-10",
                        "rodada-11",
                        "rodada-12",
                        "rodada-13",
                        "rodada-14",
                        "rodada-15"]

class BaseHandler(tornado.web.RequestHandler):
    def get_current_user(self):
        return self.get_secure_cookie("user")

    def set_default_headers(self):        
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "*")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS')

    def options(self):
        # no body
        self.set_status(204)
        self.finish()

    def put(self):
        self.write('some put')

    def post(self):
        self.write('some post')

    def get(self):
        self.write('some get')

class RodadasHandler(BaseHandler):
    def get(self):
        URL = 'https://github.com/henriquepgomide/caRtola/tree/master/data/2020'
        html = requests.get(URL)

        # Criar objeto BeautifulSoup para raspar urls 
        soup = BeautifulSoup(html.text, 'lxml')

        global rodadas_cadastradas
        rodadas = {}
        jogadores = []
        for tag in soup.find_all('a', href=True):
            if (str(tag)).find('title="rodada-') != -1:
                nomeRodada = tag.get('title').replace(".csv","")
                rodadas[nomeRodada] = "https://raw.githubusercontent.com" + tag.get('href').replace("/blob","")
                if nomeRodada not in rodadas_cadastradas:
                    html = requests.get(rodadas[nomeRodada])
                    html = (html.text).replace('"','').split("\n")
                    for line in html:
                        if line != "":
                            #resposta = request.post("url", body)
                            l = line.split(",")
                            if l[0] != "":
                                jogador = {  #criacao de um jogador 
                                        "atletas.nome":l[1],
                                        "atletas.atleta_id":l[5],
                                        "atletas.clube":l[15],
                                        "atletas.posicao": l[8],
                                        "atletas.posicao_id": l[6],
                                        "G_gols_campeonato": l[26],
                                        "atletas.pontos": l[10],
                                        "atletas.preco": l[11],
                                        "atletas.variacao": l[12],
                                        "atletas.media": l[13],
                                        "atletas.jogos": l[14],
                                        "CA_Cartao_amarelo": l[17],
                                        "CV_Cartao_vermelho": l[33],
                                        "DD_Defesa_dificil": l[29],
                                        "FC_Falta_cometida": l[19],
                                        "FD_Finalizacao_defenfida": l[23],
                                        "FS_Falta_sofrida": l[20],
                                        "PP_Penalti_perdido": l[28],
                                        "PI_Passe_incompleto": l[21],
                                        "SG_Sem_sofrer_Gol": l[22],
                                        "DS_Desarme": l[18],
                                        "FF_Finalizacao_fora": l[24],
                                        "FT_Finalizacao_trave": l[25],
                                    }
                                jogadores.append(jogador) #adiciona o jogador no final da lista jogador
                            
                            #print(nomeRodada + ": " + l[1] + " - " + l[15]) #print do nome da rodada e buscando pela posicao do atleta na lista
                    rodadas_cadastradas.append(nomeRodada) #adiciona o nome da nova rodada a lista de rodadas_cadastradas

        self.write(json.dumps(jogadores))


def make_app():
    return tornado.web.Application([
        (r"/rodadas", RodadasHandler),
    ], cookie_secret="lkasdjfoiwerlkasfmfsdjfoasjf3")


if __name__ == "__main__":
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    print("Auth : init")
    app = make_app()
    app.listen(int(os.environ.get("PORT", 5000)))
    tornado.ioloop.IOLoop.current().start() 
